"""
"""
import numpy as np
from scipy.spatial import distance
from dataclasses import dataclass
from typing import List
from more_itertools import pairwise
import matplotlib.pyplot as plt

class TSP:
    """
    Describes a TSP
    """
    def __init__(self, coordinates, *distances):
        self.places = [Place(id, v[0], v[1]) for id, v in enumerate(coordinates)]
        self.distances = distance.cdist(coordinates, coordinates, 'euclidean')

    def cost(self, place, another_place):
        return self.distances[place.id][another_place.id]

    def total_cost(self, tour):
        total_cost = 0.0
        for current, next_city in tour:
            total_cost += self.cost(current, next_city)

        return total_cost
    
    # ojo que aquí tienes error
    def missing(self,visited_places):
        return set(self.places) - set(visited_places)

    @classmethod
    def from_files(cls,coordinates_file, distances_file):
        coordinates = np.loadtxt(coordinates_file)
        distances = np.loadtxt(distances_file)
        return cls(coordinates,distances)

    @classmethod
    def from_random(cls,num_places=50, max_distance=100):
        coordinates = np.random.randint(
            low=0, high=max_distance, size=(num_places,2))
        distances = distance.cdist(coordinates, coordinates, "euclidean")
        return cls(coordinates, distances)

    def plot_cities(self):
    #    plt.scatter(cities[:,0], cities[:,1])
        plt.plot(self.places[:,0], self.places[:,1], 'co')
        plt.xlim(0, 100) #max_distance
        plt.ylim(0, 100) #max_distance
        plt.show()



@dataclass(eq=True,frozen=True)
class Place:
    """A place to be visited"""
    id: int
    x: float
    y: float

class Tour:
    """
    """
    def __init__(self):
        self._path = []

    @property
    def initial(self):
        return self._path[0]

    @property
    def current(self):
        return self._path[-1]

    @property
    def closed(self):
        closed = False
        if len(self._path) > 1:
            closed = self._path[0] == self._path[-1]

        return closed

    def close(self):
        if len(self._path) > 1:
            self._path.append(self._path[0])

    def missing(self, places):
        return set(places) -  set(self._path)

    def append(self, place):
        self._path.append(place)
        
    def visited_places(self):
        self.__path
        
    def plot_solution(self):
        plt.plot(self.places[:,0], self.places[:,1], 'co')
        plt.xlim(0, 100)
        plt.ylim(0, 100)

        for (_from, to) in pairwise(self.__path):
            plt.arrow(self.places[_from][0], self.places[_from][1],
                      self.places[to][0]- self.places[_from][0], self.places[to][1] - self.places[_from][1],
                      color='b', length_includes_head=True)


        # Close the loop
            last = self.places[-1]
            first = self.places[0]
            plt.arrow(self.places[last][0], self.places[last][1],
                      self.places[first][0]- self.places[last][0], self.places[first][1] - self.places[last][1],
                      color='b', length_includes_head=True)
            plt.show()


    def __len__(self):
        return len(self._path)


    def __iter__(self):
        return ((current, next) for (current,next) in pairwise(self._path))


    def __repr__(self):
        return f"Tour: [{'->'.join([str(place) for place in self._path])}]"


    def __str__(self):
        return f"Tour: [{'->'.join([place.id for place in self._path])}]"

__version__ = '0.1.0'


#from .cli import cli
